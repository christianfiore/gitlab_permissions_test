# Create local values to retrieve items from CSVs
locals {
  # Parse group member files
  group_members_path = "group-members"
  group_members_files = {
    for file in fileset(local.group_members_path, "*.csv") :
    trimsuffix(file, ".csv") => csvdecode(file("${local.group_members_path}/${file}"))
  }
  # Create temp object that has group ID and CSV contents
  group_members_temp = flatten([
    for group, members in local.group_members_files : [
      for grp, g in gitlab_group.all : {
        name = g.name
        id = g.id
        slug = g.slug
        members = members
      } if g.slug == group
    ]
  ])

  # Create object for each group-user relationship
  group_members = flatten([
    for group in local.group_members_temp : [
      for member in group.members : {
        name = "${group.slug}-${member.username}"
        group_id = group.id
        username = member.username
        role = member.role
      }
    ]
  ])

  # Parse repo group membership files
  repo_group_path = "repos-group"
  repo_group_files = {
    for file in fileset(local.repo_group_path, "*.csv") :
    trimsuffix(file, ".csv") => csvdecode(file("${local.repo_group_path}/${file}"))
  }
}
