resource "gitlab_group" "all" {
  for_each = {
    for group in csvdecode(file("groups.csv")) :
    group.name => group
  }

  name = each.value.name
  description = each.value.description
  visibility_level = each.value.privacy
  create_default_maintainer = true
}

resource "gitlab_group_membership" "members" {
  for_each = { for grp in local.group_members : grp.name => grp }

  group_id  = each.value.group_id
  user_user_id = each.value.username
  accessaccess_level = each.value.role
}
