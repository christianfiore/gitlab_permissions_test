# Create infrastructure repository
resource "gitlab_repository" "infrastructure" {
  name = "learn-tf-infrastructure"
}

# Add memberships for infrastructure repository
resource "gitlab_group_repository" "infrastructure" {
  for_each = {
    for group in local.repo_groups_files["infrastructure"] :
    group.group_name => {
      group_id = gitlab_group.all[group.group_name].id
      permission = group.permission
    } if lookup(gitlab_group.all, group.group_name, false) != false
  }

  group_id = each.value.group_id
  repository = gitlab_repository.infrastructure.id
  permission = each.value.permission
}

# Create application repository
resource "gitlab_repository" "application" {
  name = "learn-tf-application"
}

# Add memberships for application repository
resource "gitlab_group_repository" "application" {
  for_each = {
    for group in local.repo_groups_files["application"] :
    group.group_name => {
      group_id = gitlab_group.all[group.group_name].id
      permission = group.permission
    } if lookup(gitlab_group.all, group.group_name, false) != false
  }

  group_id = each.value.group_id
  repository = gitlab_repository.application.id
  permission = each.value.permission
}
