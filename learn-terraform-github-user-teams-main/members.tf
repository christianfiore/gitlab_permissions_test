resource "gitlab_users" "all" {
  for_each = {
    for user in csvdecode(file("members.csv")) :
    user.username => user
  }

  username = each.value.username
  role = each.value.role
}